FROM openjdk:8-alpine

COPY target/uberjar/poised.jar /poised/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/poised/app.jar"]
