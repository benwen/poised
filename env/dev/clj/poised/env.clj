(ns poised.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [poised.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[poised started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[poised has shut down successfully]=-"))
   :middleware wrap-dev})
